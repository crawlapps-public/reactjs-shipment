export const Fallback = ({ error, componentStack, resetErrorBoundary }) => {
    return (
      <>
        <h1>Something went wrong: {error.message}</h1>
        <button onClick={resetErrorBoundary}>Try again</button>
      </>
    );
};