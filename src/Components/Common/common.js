import { useEffect } from "react";

// -------------------- outside click
export const handleClickOutside = (event, wrapperRef, setOpenPop) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        if (event.target.id !== 'more_menu') {
            setOpenPop(false);
        }
    }
};

export const useOutsideClick = (wrapperRef, setOpenPop) => {
    useEffect(() => {
        const handleOutsideClick = (event) =>
            handleClickOutside(event, wrapperRef, setOpenPop);

        document.addEventListener("mousedown", handleOutsideClick);

        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };
    }, [wrapperRef, setOpenPop]);
};

// -------------------- static json
export const salesEnquiryData = [
    {
        id: 1,
        title: 'Total Enquires',
        rate: '50',
        compare_rate: '21',
        rate_type: 'down'
    },
    {
        id: 2,
        title: 'Pending Enquires',
        rate: '21',
        compare_rate: '4',
        rate_type: 'up'
    },
    {
        id: 3,
        title: 'Enquires Actioned',
        rate: '16',
        compare_rate: '3',
        rate_type: 'up'
    },
    {
        id: 4,
        title: 'SLA Branched',
        rate: '13',
        compare_rate: '6',
        rate_type: 'up'
    },
]

export const quotSumData = [
    {
        id: 1,
        title: 'Quotation Sent',
        rate: '50',
        compare_rate: '21',
        rate_type: 'down'
    },
    {
        id: 2,
        title: 'Quotation Won',
        rate: '21',
        compare_rate: '04',
        rate_type: 'up'
    },
    {
        id: 3,
        title: 'Quotation Lost',
        rate: '16',
        compare_rate: '3',
        rate_type: 'up'
    },
    {
        id: 4,
        title: 'Quotation In progress',
        rate: '13',
        compare_rate: '6',
        rate_type: 'up'
    },
]

// -------------------------- Table Data --------------------------------------------------
export const impExColumnData = [
    { label: "Port" },
    { label: "Total Enquires" },
    { label: "Conversion Ratio" },
    { label: "Trend" },
]
export const inquiryColumnData = [
    { label: "Customer" },
    { label: "Total Enquires" },
    { label: "Conversion Ratio" },
    { label: "Trend" },
]
export const salesColumnData = [
    { label: "Employee" },
    { label: "Total Enquires" },
    { label: "Conversion Ratio" },
    { label: "Trend" },
]

export const exportSumData = [
    {
        id: 1,
        port: 'Port of Houston',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 2,
        port: 'Port of South Louisiana',
        total_equiry: '456',
        ratio: '21',
        trend: '12'
    },
    {
        id: 3,
        port: 'Port of Corpus Christi',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 4,
        port: 'Port of New Orleans',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 5,
        port: 'Port of Long Beach',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 6,
        port: 'Plaquemines Port',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
]
export const salesPerformData = [
    {
        id: 1,
        employee: 'Leslie Alexander',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 2,
        employee: 'Leslie Alexander',
        total_equiry: '456',
        ratio: '21',
        trend: '12'
    },
    {
        id: 3,
        employee: 'Leslie Alexander',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 4,
        employee: 'Leslie Alexander',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 5,
        employee: 'Leslie Alexander',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
    {
        id: 6,
        employee: 'Leslie Alexander',
        total_equiry: '279',
        ratio: '21',
        trend: '12'
    },
]