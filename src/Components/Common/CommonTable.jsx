import React from 'react'

import profImg from '../../Assets/images/avatar.png'

const CommonTable = ({ column, data, type }) => {
    return (
        <>
            <div className="table_wrap">
                <table cellPadding={0} cellSpacing={0}>
                    <thead>
                        <tr>
                            {column?.map((item, index) => (
                                <th key={index}>{item.label}</th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {data?.map((item) => (
                            <tr key={item?.id}>
                                {type === 'salesPerformance' ? (
                                    <td>
                                        <div className="prof_wrap">
                                            <div className="img">
                                                <img src={profImg} alt="Profile" />
                                            </div>
                                            <span>{item?.employee}</span>
                                        </div>
                                    </td>
                                ) : (
                                    <td>{item?.port}</td>
                                )}
                                <td>{item?.total_equiry}</td>
                                <td>{item?.ratio}%</td>
                                <td><span className="green_text">{item?.trend}%</span></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default CommonTable
