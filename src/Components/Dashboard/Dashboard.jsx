import React from 'react'
import CommonTable from '../Common/CommonTable'
import { exportSumData, impExColumnData, inquiryColumnData, quotSumData, salesColumnData, salesEnquiryData, salesPerformData } from '../Common/common'
import ContainerTrackChart from './Partials/ContainerTrackChart'
import MainDnd from './Partials/MainDnd'
import RevenueChart from './Partials/RevenueChart'

function Dashboard() {
    return (
        <>
            <div className="main_dashboard_wrapper">
                <h2 className="title">Dashboard</h2>
                <div className="dashboard_inquiries_summary_wrap">
                    <div className="left_summary_wrap">
                        {/* Sales Enquiry */}
                        <div className="sh_inquiry_wrap">
                            <h3 className="sub_title">Sales Enquires</h3>
                            <div className="sh_box_wrap">
                                {salesEnquiryData?.map(item => (
                                    <div className="sh_box" key={item?.id}>
                                        <p className="box_title">{item?.title}</p>
                                        <p className="sh_inquiry_rate">{item?.rate}
                                            <span className={`${item?.rate_type === 'down' ? 'red_text' : 'green_text'}`}>{item?.compare_rate}%</span>
                                        </p>
                                    </div>
                                ))}
                            </div>
                        </div>

                        {/* Quatation */}
                        <div className="sh_inquiry_wrap">
                            <h3 className="sub_title">Quotation summary</h3>
                            <div className="sh_box_wrap">
                                {quotSumData?.map(item => (
                                    <div className="sh_box" key={item?.id}>
                                        <p className="box_title">{item?.title}</p>
                                        <p className="sh_inquiry_rate">{item?.rate}
                                            <span className={`${item?.rate_type === 'down' ? 'red_text' : 'green_text'}`}>{item?.compare_rate}%</span>
                                        </p>
                                    </div>
                                ))}
                            </div>
                        </div>

                        {/* Container Tracking Charts */}
                        <ContainerTrackChart />

                        {/* Revenue Run Rate charts */}
                        <RevenueChart />
                    </div>
                
                    {/* Right draggble Summary */}
                    <div className="sh_right_summary_wrap">
                        <MainDnd />
                    </div>
                </div>

                {/* dashboard tables */}
                <div className="sh_dashboar_table_wrap">

                    {/* Export Summary table */}
                    <div className="sh_summary_table_wrap">
                        <h3 className="sub_title">Export Summary</h3>
                        <CommonTable column={impExColumnData} data={exportSumData} />
                    </div>

                    {/* Import Summary table */}
                    <div className="sh_summary_table_wrap">
                        <h3 className="sub_title">Import Summary</h3>
                        <CommonTable column={impExColumnData} data={exportSumData} />
                    </div>

                    {/* Enquiry Summary table */}
                    <div className="sh_summary_table_wrap">
                        <h3 className="sub_title">Enquiry Summary</h3>
                        <CommonTable column={inquiryColumnData} data={exportSumData} />
                    </div>

                    {/* Sales Performance table */}
                    <div className="sh_summary_table_wrap">
                        <h3 className="sub_title">Sales Performance</h3>
                        <CommonTable column={salesColumnData} data={salesPerformData} type={'salesPerformance'} />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Dashboard
