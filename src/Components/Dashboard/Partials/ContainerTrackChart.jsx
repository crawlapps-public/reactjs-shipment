import React, { useRef, useState } from 'react';
import mapimage from '../../../Assets/images/map.svg';
import { useOutsideClick } from '../../Common/common';

const ContainerTrackChart = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [dropId, setDropId] = useState(false);
    const [labelText, setLabelText] = useState('Delivered');
    const dropdownRef = useRef();

    const dropHandler = (label) => {
        setLabelText(label);
        setIsOpen(false);
    };

    // ------------ custom dropdown -------------------
    const toggleDropdown = (id) => {
        setIsOpen(!isOpen);
        setDropId(id);
    };
    useOutsideClick(dropdownRef, setIsOpen);
    return (
        <>
            <div className="sh_container_tracking_wrap">
                <div className="title_select_wrap">
                    <h3 className="sub_title">Container Tracking</h3>
                    <div className="common_dropdown">
                        <span>Status:</span>
                        <button id='more_menu' onClick={() => { toggleDropdown('rate') }}>
                            {labelText}
                        </button>
                        {isOpen && dropId === 'rate' && (
                            <ul className='dropdown_wrap' ref={dropdownRef}>
                                <li onClick={() => { dropHandler('Delivered') }}>Delivered</li>
                                <li onClick={() => { dropHandler('Stuck') }}>Stuck</li>
                                <li onClick={() => { dropHandler('In Transit') }}>In Transit</li>
                            </ul>
                        )}
                    </div>
                </div>

                {/* Charts Details */}
                <div className="chart_info_wrap">
                    <div className="chart_wrap">
                        <img src={mapimage} alt="Charts" />
                    </div>
                    <div className="map_info_wrap">
                        <ul>
                            <li className='active'>
                                <div className="map_info">
                                    <span className="status delivered">Delivered</span>
                                    <p className="name">Maria Jenson</p>
                                    <p className="location">Port of Houston</p>
                                </div>
                            </li>
                            <li>
                                <div className="map_info">
                                    <span className="status stuck">Stuck</span>
                                    <p className="name">Vironica lexus</p>
                                    <p className="location">Port of Long Beach</p>
                                </div>
                            </li>
                            <li>
                                <div className="map_info">
                                    <span className="status transit">In Transit</span>
                                    <p className="name">Mia toreto</p>
                                    <p className="location">Port of New Orleans</p>
                                </div>
                            </li>
                            <li>
                                <div className="map_info">
                                    <span className="status transit">In Transit</span>
                                    <p className="name">Mia toreto</p>
                                    <p className="location">Port of New Orleans</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContainerTrackChart
