import React, { useEffect, useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { taskStatus } from './initialData';

const onDragEnd = (result, columns, setColumns) => {
    if (!result.destination) return;
    const { source, destination } = result;
  
    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems
        }
      });
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems
        }
      });
    }
};

export default function MainDnd() {
    const [columns, setColumns] = useState({});
    useEffect(() => {
        setColumns(taskStatus);
    },[])
    console.log(columns,"columns");
    return (
        <>
            <DragDropContext onDragEnd={(result) => onDragEnd(result, columns, setColumns)} >
            {Object.entries(columns).map(([columnId, column], index) => {
                return (
                <div className='sh_revenue_summary_wrap' key={columnId} >

                    <h3 className="sub_title" dangerouslySetInnerHTML={{__html:column.name}} />

                    <Droppable droppableId={columnId} key={columnId}>
                        {(provided, snapshot) => {
                        return (
                            <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                            >
                            {column.items.map((item, index) => {
                                return (
                                <Draggable key={item.id} draggableId={item.id} index={index} >                                       
                                    {(provided, snapshot) => {
                                    return (
                                        <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        className='sh_summary_box'
                                        >
                                            <p className='sh_summ_title'>{item?.title}</p>
                                            <p className='sh_sum_rate'>{item?.revenue} 
                                                <span className={`${item?.rate_type === 'down' ? 'red_text' : 'green_text'}`}>{item?.rate}%</span>
                                            </p>
                                        </div>
                                    );
                                    }}
                                </Draggable>
                                );
                            })}
                            {provided.placeholder}
                            </div>
                        );
                        }}
                    </Droppable>
                </div>
                );
            })}
            </DragDropContext>
        </>
    )
}
