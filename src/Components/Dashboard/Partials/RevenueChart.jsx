import React, { useRef, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { useOutsideClick } from '../../Common/common';

const RevenueChart = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [dropId, setDropId] = useState(false);
    const [labelText, setLabelText] = useState('Annual Run Rate');
    const dropdownRef = useRef();

    const dropHandler = (label) => {
        setLabelText(label);
        setIsOpen(false);
    };

    // ------------ custom dropdown -------------------
    const toggleDropdown = (id) => {
        setIsOpen(!isOpen);
        setDropId(id);
    };
    useOutsideClick(dropdownRef, setIsOpen);

    // ------------------------ charts options-------------------------
    const options = {
        chart: {
            height: 270,
            type: 'bar',
            toolbar: {
                show: false,
            },
        },
        plotOptions: {
            bar: {
                columnWidth: "20",
            }
        },
        dataLabels: {
            enabled: false
        },
        grid: {
            borderColor: "#E6E6E6",
            strokeDashArray: 2,
        },
        xaxis: {
            labels: {
                rotate: -45,
                style: {
                    fontWeight: "600",
                    fontSize: "12px",
                    color: ["#000000"],
                },
            },
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
            ],
            tickPlacement: 'on'
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'light',
                type: "horizontal",
                shadeIntensity: 0.25,
                colorStops: [
                    {
                        offset: 40,
                        color: "#F2993D",
                        opacity: 1
                    },
                    {
                        offset: 100,
                        color: "#F2993D",
                        opacity: 0.2
                    }

                ],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 0.2,
                stops: [50, 0, 100]
            },
        }
    };
    const series = [{
        name: 'Revenue',
        data: [44, 55, 41, 67, 35, 43, 50, 55, 45, 40, 87, 65]
    }]
    return (
        <>
            <div className="sh_revenue_chart_wrap">

                <div className="title_select_wrap">
                    <h3 className="sub_title">Revenue Run Rate</h3>
                    <div className="common_dropdown">
                        <button id='more_menu' onClick={() => { toggleDropdown('rate') }}>
                            {labelText}
                        </button>
                        {isOpen && dropId === 'rate' && (
                            <ul className='dropdown_wrap' ref={dropdownRef}>
                                <li onClick={() => { dropHandler('Annual Run Rate') }}>Annual Run Rate</li>
                                <li onClick={() => { dropHandler('Monthly Run Rate') }}>Monthly Run Rate</li>
                            </ul>
                        )}
                    </div>
                </div>

                {/* Revenue Chart */}
                <div className="chart_wrap">
                    <ReactApexChart options={options} series={series} type="bar" height={270} />
                </div>
            </div>
        </>
    )
}

export default RevenueChart
