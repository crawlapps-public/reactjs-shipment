import React from 'react'

import dashboardIcon from '../../Assets/images/dashboard_icon.svg';
import shipmentIcon from '../../Assets/images/shipment_icon.svg';
import quotationIcon from '../../Assets/images/quotations_icon.svg';
import userIcon from '../../Assets/images/users_icon.svg';
import documentIcon from '../../Assets/images/document_icon.svg';
import settingIcon from '../../Assets/images/settings_icon.svg';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {
    const menu = [
        {
            name: 'Dashboard',
            icon: dashboardIcon,
            routerlink: '/'
        },
        {
            name: 'Shipments',
            icon: shipmentIcon,
            routerlink: '/shipments'
        },
        {
            name: 'Quotations',
            icon: quotationIcon,
            routerlink: '/quotation'
        },
        {
            name: 'Users',
            icon: userIcon,
            routerlink: '/users'
        },
        {
            name: 'Documents',
            icon: documentIcon,
            routerlink: '/documents'
        },
        {
            name: 'Settings',
            icon: settingIcon,
            routerlink: '/settings'
        },
    ]
    return (
        <>
            <div className="sidebar_wrap">
                <div className="sidemenu_wrap">
                    <ul>
                        {
                            menu.map((item, index) =>
                                <li key={index}>
                                    <NavLink activeclassname="active" to={item.routerlink}>
                                        <img src={item.icon} alt="" />
                                        {item.name}
                                    </NavLink >
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>
        </>
    )
}

export default Sidebar
