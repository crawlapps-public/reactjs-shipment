import React, { useRef, useState } from 'react';
import { Link } from 'react-router-dom';

import logo from '../../Assets/images/logo.svg';
import langimg from '../../Assets/images/lang.svg';
import searchIcon from '../../Assets/images/search.svg';
import commentIcon from '../../Assets/images/comment.svg';
import notificationIcon from '../../Assets/images/notification.svg';
import profileImg from '../../Assets/images/avatar.png';
import { useOutsideClick } from '../Common/common';

const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [dropId, setDropId] = useState(false);
    const dropdownRef = useRef();

    // ------------ custom dropdown -------------------
    const toggleDropdown = (id) => {
        setIsOpen(!isOpen);
        setDropId(id);
    };
    useOutsideClick(dropdownRef, setIsOpen);
    // ------------ custom dropdown -------------------
    return (
        <>
            <header>
                <div className="logo">
                    <Link to={'/'}>
                        <img src={logo} alt="Logo" />
                    </Link>
                </div>
                <div className="header_right_info">
                    <div className="common_dropdown language_dropdown">
                        <button id='more_menu' onClick={() => { toggleDropdown('lang') }}>
                            <img src={langimg} alt="Language" />
                            New York
                        </button>
                        {isOpen && dropId === 'lang' && (
                            <ul className='dropdown_wrap' ref={dropdownRef}>
                                <li onClick={() => {setIsOpen(false)}}><img src={langimg} alt="Language" /> New York</li>
                                <li onClick={() => {setIsOpen(false)}}><img src={langimg} alt="Language" /> New York</li>
                                <li onClick={() => {setIsOpen(false)}}><img src={langimg} alt="Language" /> New York</li>
                                <li onClick={() => {setIsOpen(false)}}><img src={langimg} alt="Language" /> New York</li>
                            </ul>
                        )}
                    </div>
                    <div className="account_info_wrap">
                        <div className="search_wrap global_search_wrap">
                            <div className="searchbox">
                                <input type="search" placeholder="Search" />
                                <div className="searchIcon_wrap">
                                    <img src={searchIcon} className="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="comment_wrap">
                            <button><img src={commentIcon} alt="Comment" /></button>
                        </div>
                        <div className="notification_wrap">
                            <button><img src={notificationIcon} alt="Notification" /></button>
                        </div>
                        <div className="profile_wrap">
                            <button id='more_menu' className="img"><img src={profileImg} alt="Profile" /></button>                            
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}

export default Header
