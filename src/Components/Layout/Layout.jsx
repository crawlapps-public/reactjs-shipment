import React from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import { Outlet } from 'react-router-dom'

const Layout = () => {
    return (
        <>
            <Header />
            <div className="main_wrapper">
                <Sidebar />
                <div className="mainOutlet">
                    <Outlet />
                </div>
            </div> 
        </>
    )
}

export default Layout
