import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Fallback } from "./Utils/Fallback";
import Dashboard from "./Components/Dashboard/Dashboard";
import Layout from "./Components/Layout/Layout";
import './Assets/scss/main.scss'
import Shipment from "./Components/Shipment/Shipment";
import Quotations from "./Components/Quotations/Quotations";
import Documents from "./Components/Documents/Documents";
import Users from "./Components/Users/Users";
import Settings from "./Components/Settings/Settings";

function App() {
  const errorhandler = (errors, errorInfo) => {
    console.log("Logging", errors, errorInfo);
  };
  return (
    <>
      <ErrorBoundary FallbackComponent={Fallback} onError={errorhandler}>
        <BrowserRouter>
          <Routes>
            <Route element={<Layout />}>
                <Route path="/" title="Dashboard" element= {<Dashboard />} />
                <Route path="/shipments" title="Shipments" element= {<Shipment />} />
                <Route path="/quotation" title="Quotations" element= {<Quotations />} />
                <Route path="/users" title="Users" element= {<Users />} />
                <Route path="/documents" title="Documents" element= {<Documents />} />
                <Route path="/settings" title="Settings" element= {<Settings />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </ErrorBoundary>
    </>
  );
}

export default App;
